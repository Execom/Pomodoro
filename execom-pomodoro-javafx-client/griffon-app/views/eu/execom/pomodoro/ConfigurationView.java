package eu.execom.pomodoro;

import javax.annotation.Nonnull;

import org.codehaus.griffon.runtime.javafx.artifact.AbstractJavaFXGriffonView;
import org.kordamp.ikonli.fontawesome.FontAwesome;
import org.kordamp.ikonli.javafx.FontIcon;

import griffon.core.artifact.GriffonView;
import griffon.inject.MVCMember;
import griffon.metadata.ArtifactProviderFor;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;

@ArtifactProviderFor(GriffonView.class)
public class ConfigurationView extends AbstractJavaFXGriffonView {

  private ConfigurationController controller;

  private ConfigurationModel model;

  private ExecomPomodoroJavafxClientView parentView;

  @FXML
  private Label clickLabel;

  @MVCMember
  public void setController(@Nonnull ConfigurationController controller) {
    this.controller = controller;
  }

  @MVCMember
  public void setModel(@Nonnull ConfigurationModel model) {
    this.model = model;
  }

  @MVCMember
  public void setParentView(@Nonnull ExecomPomodoroJavafxClientView parentView) {
    this.parentView = parentView;
  }

  @Override
  public void initUI() {
    Tab tab = new Tab("Configuration");
    tab.setGraphic(new FontIcon(FontAwesome.COGS));
    tab.setClosable(false);
    Node node = loadFromFXML();
    connectActions(node, controller);
    connectMessageSource(node);
    tab.setContent(node);
    parentView.getTabPane().getTabs().add(tab);
    model.clickCountProperty().bindBidirectional(clickLabel.textProperty());
  }
}
