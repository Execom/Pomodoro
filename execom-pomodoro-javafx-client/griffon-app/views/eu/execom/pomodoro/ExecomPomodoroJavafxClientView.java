package eu.execom.pomodoro;

import java.util.Collections;
import java.util.Map;

import javax.annotation.Nonnull;

import org.codehaus.griffon.runtime.javafx.artifact.AbstractJavaFXGriffonView;

import griffon.core.artifact.GriffonView;
import griffon.inject.MVCMember;
import griffon.metadata.ArtifactProviderFor;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

@ArtifactProviderFor(GriffonView.class)
public class ExecomPomodoroJavafxClientView extends AbstractJavaFXGriffonView {

  private TabPane tabPane;

  private ExecomPomodoroJavafxClientController controller;
  private ExecomPomodoroJavafxClientModel model;

  @FXML
  private Label clickLabel;

  @MVCMember
  public void setController(@Nonnull ExecomPomodoroJavafxClientController controller) {
    this.controller = controller;
  }

  @MVCMember
  public void setModel(@Nonnull ExecomPomodoroJavafxClientModel model) {
    this.model = model;
  }

  @Override
  public void mvcGroupInit(Map<String, Object> args) {
    createMVCGroup("Pomodoro");
    createMVCGroup("Configuration");
  }

  @Override
  public void initUI() {
    Stage stage = (Stage) getApplication().createApplicationContainer(Collections.<String, Object>emptyMap());
    stage.setTitle(getApplication().getConfiguration().getAsString("application.title"));
    stage.setScene(init());
    stage.sizeToScene();
    getApplication().getWindowManager().attach("mainWindow", stage);
  }

  private Scene init() {
    tabPane = new TabPane();
    Scene scene = new Scene(tabPane);
    return scene;
  }

  public TabPane getTabPane() {
    return tabPane;
  }

  public void setTabPane(TabPane tabPane) {
    this.tabPane = tabPane;
  }

}
