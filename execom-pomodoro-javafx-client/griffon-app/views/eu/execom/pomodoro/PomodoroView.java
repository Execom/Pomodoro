package eu.execom.pomodoro;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import org.codehaus.griffon.runtime.javafx.artifact.AbstractJavaFXGriffonView;
import org.kordamp.ikonli.fontawesome.FontAwesome;
import org.kordamp.ikonli.javafx.FontIcon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.execom.pomodoro.timer.PomodoroTimer;
import griffon.core.artifact.GriffonView;
import griffon.inject.MVCMember;
import griffon.metadata.ArtifactProviderFor;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.shape.Circle;

@ArtifactProviderFor(GriffonView.class)
public class PomodoroView extends AbstractJavaFXGriffonView {

  private PomodoroController controller;
  private PomodoroModel model;
  private ExecomPomodoroJavafxClientView parentView;

  private Logger logger = LoggerFactory.getLogger(getClass());

  @FXML
  private Circle currentColorCircle;
  @FXML
  private Button clickStart;
  @FXML
  private Button clickStop;
  @FXML
  private Label timerLabel;

  @MVCMember
  public void setController(@Nonnull PomodoroController controller) {
    this.controller = controller;
  }

  @MVCMember
  public void setModel(@Nonnull PomodoroModel model) {
    this.model = model;
  }

  @MVCMember
  public void setParentView(@Nonnull ExecomPomodoroJavafxClientView parentView) {
    this.parentView = parentView;
  }

  @Override
  public void initUI() {
    Node node = loadFromFXML();
    connectActions(node, controller);
    Tab tab = new Tab("Pomodoro");
    tab.setGraphic(new FontIcon(FontAwesome.WINDOWS));
    tab.setClosable(false);

    connectActions(node, controller);
    connectMessageSource(node);
    tab.setContent(node);
    parentView.getTabPane().getTabs().add(tab);
    model.timerProperty().bindBidirectional(timerLabel.textProperty());
  }

  private void startTimer() {
    long min = 1;
    logger.info("Initial value: {} milliseconds", TimeUnit.MINUTES.toMillis(min));
    PomodoroTimer timer = PomodoroTimer.of(TimeUnit.MINUTES.toMillis(min), (tc) -> {
      logger.info("Minutes {}, Seconds {}", tc.getMinutes(), tc.getSeconds());
      timerLabel.setText(String.format("%d:%d", tc.getMinutes(), tc.getSeconds()));
    }, (ev) -> {
      logger.info("I finished!");
    });
    timer.start();
  }

}
