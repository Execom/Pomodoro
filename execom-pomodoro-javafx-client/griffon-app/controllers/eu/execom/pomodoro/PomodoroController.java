package eu.execom.pomodoro;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.codehaus.griffon.runtime.core.artifact.AbstractGriffonController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wolkabout.wolk.Device;
import com.wolkabout.wolk.Wolk;

import eu.execom.pomodoro.statemachine.StateMachine;
import eu.execom.pomodoro.statemachine.pomodoro.PomodoroStateMachine;
import eu.execom.pomodoro.statemachine.pomodoro.StartTimerEvent;
import eu.execom.pomodoro.statemachine.pomodoro.StopTimerEvent;
import eu.execom.pomodoro.statemachine.pomodoro.TimerFinishEvent;
import eu.execom.pomodoro.timer.PomodoroTimer;
import eu.execom.pomodoro.timer.Timer;
import griffon.core.artifact.GriffonController;
import griffon.inject.MVCMember;
import griffon.metadata.ArtifactProviderFor;
import griffon.transform.Threading;

@ArtifactProviderFor(GriffonController.class)
public class PomodoroController extends AbstractGriffonController {

  private Logger logger = LoggerFactory.getLogger(getClass());

  private PomodoroModel model;

  @Inject
  private PomodoroService pomodoroService;

  @Inject
  private WolkService wolkService;

  @Inject
  private StateMachine stateMachine;

  @MVCMember
  public void setModel(@Nonnull PomodoroModel model) {
    this.model = model;
  }

  private void createTimers() {
    logger.info("Current state {}", stateMachine.getCurrentState().getClass().getName());
    logger.debug("Creating timer");
    Timer busyTimer = PomodoroTimer.of(TimeUnit.MINUTES.toMillis(25), (counter) -> {
      logger.info("Available Timer: {}", String.format("%d:%d", counter.getMinutes(), counter.getSeconds()));
      model.setTimer(String.format("%d:%d", counter.getMinutes(), counter.getSeconds()));
    }, (ae) -> {
      stateMachine.handleEvent(new TimerFinishEvent());
    });
    Timer availableTimer = PomodoroTimer.of(TimeUnit.MINUTES.toMillis(5), (counter) -> {
      logger.info("Busy Timer: {}", String.format("%d:%d", counter.getMinutes(), counter.getSeconds()));
      model.setTimer(String.format("%d:%d", counter.getMinutes(), counter.getSeconds()));
    }, (ae) -> {
      stateMachine.handleEvent(new TimerFinishEvent());
    });
    stateMachine.addStateResource(PomodoroStateMachine.AVAILABLE_TIMER, availableTimer);
    stateMachine.addStateResource(PomodoroStateMachine.BUSY_TIMER, busyTimer);
  }

  @PostConstruct
  public void postConstruct() {
    Device device = new Device();
    device.setSerialId("0317GD004VMF35GE ");
    device.setPassword("2xgqdc5d5fv3rngh");
    wolkService.registerWolkClient("generated", new Wolk(device));
    createTimers();
  }

  @Threading(Threading.Policy.INSIDE_UITHREAD_ASYNC)
  public void start() {
    stateMachine.handleEvent(new StartTimerEvent());
  }

  @Threading(Threading.Policy.INSIDE_UITHREAD_ASYNC)
  public void stop() {
    stateMachine.handleEvent(new StopTimerEvent());
  }
}