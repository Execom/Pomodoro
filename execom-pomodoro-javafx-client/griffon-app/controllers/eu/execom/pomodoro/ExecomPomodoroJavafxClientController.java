package eu.execom.pomodoro;

import griffon.core.artifact.GriffonController;
import griffon.inject.MVCMember;
import griffon.metadata.ArtifactProviderFor;
import org.codehaus.griffon.runtime.core.artifact.AbstractGriffonController;

import griffon.transform.Threading;
import javax.annotation.Nonnull;

@ArtifactProviderFor(GriffonController.class)
public class ExecomPomodoroJavafxClientController extends AbstractGriffonController {
  private ExecomPomodoroJavafxClientModel model;

  @MVCMember
  public void setModel(@Nonnull ExecomPomodoroJavafxClientModel model) {
    this.model = model;
  }

  @Threading(Threading.Policy.INSIDE_UITHREAD_ASYNC)
  public void click() {
    int count = Integer.parseInt(model.getClickCount());
    model.setClickCount(String.valueOf(count + 1));
  }
}