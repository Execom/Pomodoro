package eu.execom.pomodoro;

import griffon.core.artifact.GriffonService;
import griffon.metadata.ArtifactProviderFor;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.griffon.runtime.core.artifact.AbstractGriffonService;

import com.wolkabout.wolk.Wolk;

@javax.inject.Singleton
@ArtifactProviderFor(GriffonService.class)
public class WolkService extends AbstractGriffonService {

  private Map<String, Wolk> wolkClients;

  public WolkService() {
    wolkClients = new HashMap<>();
  }

  public Wolk getWolkClient(String id) {
    return wolkClients.getOrDefault(id, new Wolk(null));
  }

  public void registerWolkClient(String id, Wolk wolk) {
    wolkClients.put(id, wolk);
  }

  public Wolk deregisterWolkClient(String id, Wolk wolk) {
    return wolkClients.remove(id);
  }

}