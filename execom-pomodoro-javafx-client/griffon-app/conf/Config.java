
import griffon.util.AbstractMapResourceBundle;

import javax.annotation.Nonnull;
import java.util.Map;

import static java.util.Arrays.asList;
import static griffon.util.CollectionUtils.map;

public class Config extends AbstractMapResourceBundle {
  @Override
  protected void initialize(@Nonnull Map<String, Object> entries) {
    map(entries)
        .e("application",
            map().e("title", "execom-pomodoro-javafx-client").e("startupGroups", asList("execomPomodoroJavafxClient"))
                .e("autoShutdown", true))
        .e("mvcGroups",
            map()
                .e("execomPomodoroJavafxClient",
                    map().e("model", "eu.execom.pomodoro.ExecomPomodoroJavafxClientModel")
                        .e("view", "eu.execom.pomodoro.ExecomPomodoroJavafxClientView")
                        .e("controller", "eu.execom.pomodoro.ExecomPomodoroJavafxClientController"))
                .e("Pomodoro",
                    map().e("model", "eu.execom.pomodoro.PomodoroModel").e("view", "eu.execom.pomodoro.PomodoroView")
                        .e("controller", "eu.execom.pomodoro.PomodoroController"))
                .e("Configuration",
                    map().e("model", "eu.execom.pomodoro.ConfigurationModel")
                        .e("view", "eu.execom.pomodoro.ConfigurationView")
                        .e("controller", "eu.execom.pomodoro.ConfigurationController")));
  }
}
