package eu.execom.pomodoro;

import javax.annotation.Nonnull;

import org.codehaus.griffon.runtime.core.artifact.AbstractGriffonModel;

import griffon.core.artifact.GriffonModel;
import griffon.metadata.ArtifactProviderFor;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@ArtifactProviderFor(GriffonModel.class)
public class PomodoroModel extends AbstractGriffonModel {

  private StringProperty time;

  @Nonnull
  public final StringProperty timerProperty() {
    if (time == null) {
      time = new SimpleStringProperty(this, "time", "00:00");
    }
    return time;
  }

  public void setTimer(String timer) {
    timerProperty().set(timer);
  }

  public String getTimer() {
    return timerProperty().get();
  }

}