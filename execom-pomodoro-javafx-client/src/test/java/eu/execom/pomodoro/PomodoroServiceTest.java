package eu.execom.pomodoro;

import org.junit.Rule;
import org.junit.Test;

import griffon.core.test.GriffonUnitRule;
import griffon.core.test.TestFor;

@TestFor(PomodoroService.class)
public class PomodoroServiceTest {
   static {
   // force initialization JavaFX Toolkit
    new javafx.embed.swing.JFXPanel();
   }

  private PomodoroService service;

  @Rule
  public final GriffonUnitRule griffon = new GriffonUnitRule();

  @Test
  public void smokeTest() {
    // fail("Not yet implemented!");
  }
}