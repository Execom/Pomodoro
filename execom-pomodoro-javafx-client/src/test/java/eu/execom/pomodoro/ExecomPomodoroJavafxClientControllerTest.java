package eu.execom.pomodoro;

import javax.inject.Inject;

import org.junit.Rule;
import org.junit.Test;

import griffon.core.artifact.ArtifactManager;
import griffon.core.test.GriffonUnitRule;
import griffon.core.test.TestFor;

@TestFor(ExecomPomodoroJavafxClientController.class)
public class ExecomPomodoroJavafxClientControllerTest {
  static {
    // force initialization JavaFX Toolkit
    new javafx.embed.swing.JFXPanel();
  }

  @Inject
  private ArtifactManager artifactManager;

  private ExecomPomodoroJavafxClientController controller;

  @Rule
  public final GriffonUnitRule griffon = new GriffonUnitRule();

  @Test
  public void executeClickAction() {
    // // given:
    // ExecomPomodoroJavafxClientModel model =
    // artifactManager.newInstance(ExecomPomodoroJavafxClientModel.class);
    // controller.setModel(model);
    //
    // // when:
    // controller.invokeAction("click");
    // await().atMost(2, SECONDS);
    //
    // // then:
    // assertEquals("1", model.getClickCount());
  }
}
