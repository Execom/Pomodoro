package eu.execom.pomodoro;

import org.junit.Rule;
import org.junit.Test;

import griffon.core.test.GriffonUnitRule;
import griffon.core.test.TestFor;

@TestFor(PomodoroController.class)
public class PomodoroControllerTest {
  static {
    // force initialization JavaFX Toolkit
    new javafx.embed.swing.JFXPanel();
  }

  private PomodoroController controller;

  @Rule
  public final GriffonUnitRule griffon = new GriffonUnitRule();

  @Test
  public void smokeTest() {
    // fail("Not yet implemented!");
  }
}