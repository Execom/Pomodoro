package eu.execom.pomodoro;

import org.junit.Rule;
import org.junit.Test;

import griffon.core.test.GriffonUnitRule;
import griffon.core.test.TestFor;

@TestFor(WolkService.class)
public class WolkServiceServiceTest {
  static {
    // force initialization JavaFX Toolkit
    new javafx.embed.swing.JFXPanel();
  }

  private WolkService service;

  @Rule
  public final GriffonUnitRule griffon = new GriffonUnitRule();

  @Test
  public void smokeTest() {
    // fail("Not yet implemented!");
  }
}