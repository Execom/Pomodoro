package eu.execom.pomodoro;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;

import griffon.javafx.test.FunctionalJavaFXRunner;
import griffon.javafx.test.GriffonTestFXClassRule;

@RunWith(FunctionalJavaFXRunner.class)
public class ExecomPomodoroJavafxClientFunctionalTest {
    @ClassRule
    public static GriffonTestFXClassRule testfx = new GriffonTestFXClassRule("mainWindow");

    @Test
    public void _01_clickButton() {
    // // given:
    // verifyThat("#clickLabel", hasText("0"));
    //
    // // when:
    // testfx.clickOn("#clickActionTarget");
    //
    // // then:
    // verifyThat("#clickLabel", hasText("1"));
    }
}
