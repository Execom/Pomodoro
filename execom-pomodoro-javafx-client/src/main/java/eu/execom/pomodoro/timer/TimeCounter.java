package eu.execom.pomodoro.timer;

public interface TimeCounter {

  void increase();

  void decrease();

  long getMinutes();

  int getSeconds();

  void setValue(int value);
}
