/**
 * Holds classes that are responsible for timer in application
 * 
 * @author sbratic
 */
package eu.execom.pomodoro.timer;