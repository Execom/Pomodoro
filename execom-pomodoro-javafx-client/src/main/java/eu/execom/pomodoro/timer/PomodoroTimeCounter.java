package eu.execom.pomodoro.timer;

public class PomodoroTimeCounter implements TimeCounter {

  private long value;

  public PomodoroTimeCounter(long start) {
    super();
    this.value = start;
  }

  @Override
  public void increase() {
    value += 1000;
  }

  @Override
  public void decrease() {
    value -= 1000;
  }

  @Override
  public long getMinutes() {
    return (this.value / 1000) / 60;
  }

  @Override
  public int getSeconds() {
    return (int) ((this.value / 1000) % 60);
  }

  @Override
  public void setValue(int value) {
    this.value = value;
  }

}
