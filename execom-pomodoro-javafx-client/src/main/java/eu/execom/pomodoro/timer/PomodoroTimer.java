package eu.execom.pomodoro.timer;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

public class PomodoroTimer implements Timer {

  private Logger logger = LoggerFactory.getLogger(getClass());

  private long start;
  private final TimeCounter counter;
  private Timeline timeline;

  private PomodoroTimer(long start, OnTickCallback onTickCallback, EventHandler<ActionEvent> onFinish) {
    super();
    this.start = start;
    this.counter = new PomodoroTimeCounter(start);
    this.timeline = new Timeline(new KeyFrame(Duration.seconds(1), ev -> {
      this.counter.decrease();
      onTickCallback.apply(this.counter);
    }));
    this.timeline.setOnFinished((eh) -> {
      logger.debug("Finishing timer");
      onFinish.handle(eh);
    });
    this.timeline.setCycleCount((int) TimeUnit.MINUTES.toMillis(this.counter.getMinutes()) / 1000);
  }

  public static PomodoroTimer of(long start) {
    return of(start, (tc) -> {
    }, (ev) -> {
    });
  }

  public static PomodoroTimer of(long start, OnTickCallback onTickCallback, EventHandler<ActionEvent> onFinish) {
    PomodoroTimer pomodoroTimer = new PomodoroTimer(start, onTickCallback, onFinish);
    return pomodoroTimer;
  }

  @Override
  public void start() {
    logger.debug("public void start()");
    timeline.play();
  }

  @Override
  public void stop() {
    logger.debug("public void stop()");
    timeline.stop();
  }

  @Override
  public void reset() {
    logger.debug("public void reset()");
    setInitialValue(start);
  }

  @Override
  public void setInitialValue(long value) {
    logger.debug("public void reset()");
    this.start = value;
    this.counter.setValue((int) value);
  }

}
