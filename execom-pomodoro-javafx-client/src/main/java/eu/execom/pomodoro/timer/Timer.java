package eu.execom.pomodoro.timer;

public interface Timer {

  void start();

  void stop();

  void reset();

  void setInitialValue(long value);
}
