package eu.execom.pomodoro.timer;

@FunctionalInterface
public interface OnTickCallback {
  void apply(TimeCounter counter);
}
