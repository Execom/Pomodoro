package eu.execom.pomodoro.statemachine.pomodoro;

import eu.execom.pomodoro.statemachine.Event;

public class StopTimerEvent implements Event {

  @Override
  public String getEventModifier() {
    return Events.STOP;
  }

}
