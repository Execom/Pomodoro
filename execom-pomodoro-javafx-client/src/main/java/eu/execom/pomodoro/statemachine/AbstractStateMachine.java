package eu.execom.pomodoro.statemachine;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractStateMachine implements StateMachine {

  private Logger logger = LoggerFactory.getLogger(getClass());

  protected State currentState;

  protected Map<String, Object> stateResources;

  public AbstractStateMachine(State initialState) {
    this.currentState = initialState;
    this.stateResources = new HashMap<>();
  }

  public AbstractStateMachine(State currentState, Map<String, Object> stateResources) {
    super();
    this.currentState = currentState;
    this.stateResources = stateResources;
  }

  @Override
  public State getCurrentState() {
    return currentState;
  }

  @Override
  public void transitionTo(State state) {
    logger.info("From {} to {}", this.currentState.getClass().getSimpleName(), state.getClass().getSimpleName());
    assert currentState != null;
    assert state != null;
    currentState.onExit();
    state.onEntry();
    state.execute();
    currentState = state;
  }

  @Override
  public void handleEvent(Event event) {
    this.currentState.handleEvent(event, this);
  }

  @Override
  public Object getStateResource(String key) {
    return stateResources.get(key);
  }

  @Override
  public void addStateResource(String key, Object value) {
    stateResources.put(key, value);
  }

  @Override
  public Object removeStateResource(String key) {
    return stateResources.remove(key);
  }

}
