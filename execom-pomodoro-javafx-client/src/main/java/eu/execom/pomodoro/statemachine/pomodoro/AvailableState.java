package eu.execom.pomodoro.statemachine.pomodoro;

import eu.execom.pomodoro.statemachine.AbstractState;
import eu.execom.pomodoro.statemachine.Event;
import eu.execom.pomodoro.statemachine.StateMachine;
import eu.execom.pomodoro.timer.Timer;

public class AvailableState extends AbstractState {

  private final Timer availableTimer;

  public AvailableState(Timer availableTimer) {
    super();
    this.availableTimer = availableTimer;
  }

  @Override
  public void onEntry() {
    super.onEntry();
    availableTimer.start();
  }

  @Override
  public void onExit() {
    super.onExit();
    availableTimer.stop();
  }

  @Override
  public void handleEvent(Event event, StateMachine stateMachine) {
    switch (event.getEventModifier()) {
    case Events.STOP:
      stateMachine.transitionTo(new InitialState());
      break;
    case Events.FINISH:
      stateMachine.transitionTo(new BusyState((Timer) stateMachine.getStateResource(PomodoroStateMachine.BUSY_TIMER)));
      break;
    default:
      throw new IllegalStateException(
          String.format("Cannot trigger %s from %s state", event.getEventModifier(), getClass().getName()));
    }

  }

}
