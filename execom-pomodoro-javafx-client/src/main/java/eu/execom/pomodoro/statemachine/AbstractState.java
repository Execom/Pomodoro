package eu.execom.pomodoro.statemachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractState implements State {

  private Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void onEntry() {
    logger.debug("public void onEntry()");
  }

  @Override
  public void execute() {
    logger.debug("public void execute()");

  }

  @Override
  public void onExit() {
    logger.debug("public void onExit()");
  }

  @Override
  public void handleEvent(Event event, StateMachine stateMachine) {
    logger.debug("public void onExit() Event : {}", event.getEventModifier());
  }

}
