package eu.execom.pomodoro.statemachine.pomodoro;

import eu.execom.pomodoro.statemachine.Event;

public class StartTimerEvent implements Event {

  @Override
  public String getEventModifier() {
    return Events.START;
  }

}
