package eu.execom.pomodoro.statemachine;

@FunctionalInterface
public interface Event {

  String getEventModifier();

}
