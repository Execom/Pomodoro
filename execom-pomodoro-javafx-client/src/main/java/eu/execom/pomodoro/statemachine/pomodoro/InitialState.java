package eu.execom.pomodoro.statemachine.pomodoro;

import eu.execom.pomodoro.statemachine.AbstractState;
import eu.execom.pomodoro.statemachine.Event;
import eu.execom.pomodoro.statemachine.StateMachine;
import eu.execom.pomodoro.timer.Timer;

public class InitialState extends AbstractState {

  @Override
  public void handleEvent(Event event, StateMachine stateMachine) {

    super.handleEvent(event, stateMachine);
    switch (event.getEventModifier()) {
    case Events.START:
      stateMachine.transitionTo(new BusyState((Timer) stateMachine.getStateResource(PomodoroStateMachine.BUSY_TIMER)));
      break;
    case Events.STOP:
      stateMachine.transitionTo(new InitialState());
      break;
    default:
      throw new IllegalStateException(
          String.format("Cannot trigger %s from %s state", event.getEventModifier(), getClass().getName()));
    }
  }

}
