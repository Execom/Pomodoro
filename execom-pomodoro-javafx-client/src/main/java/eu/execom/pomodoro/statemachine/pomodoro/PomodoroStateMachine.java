package eu.execom.pomodoro.statemachine.pomodoro;

import eu.execom.pomodoro.statemachine.AbstractStateMachine;
import eu.execom.pomodoro.statemachine.State;

public class PomodoroStateMachine extends AbstractStateMachine {

  public static final String AVAILABLE_TIMER = "available";
  public static final String BUSY_TIMER = "busy";

  public PomodoroStateMachine() {
    super(new InitialState());
  }

  public PomodoroStateMachine(State initialState) {
    super(initialState);
  }

  @Override
  public void resetStateMachine() {
    this.currentState = new InitialState();
  }

}
