package eu.execom.pomodoro.statemachine;

public interface StateMachine {

  State getCurrentState();

  void transitionTo(State state);

  void handleEvent(Event event);

  Object getStateResource(String key);

  void addStateResource(String key, Object value);

  Object removeStateResource(String key);

  void resetStateMachine();
}
