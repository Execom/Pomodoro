package eu.execom.pomodoro.statemachine.pomodoro;

public class Events {

  public static final String START = "start";
  public static final String STOP = "stop";
  public static final String FINISH = "finish";

  private Events() {
    throw new UnsupportedOperationException();
  }

}
