package eu.execom.pomodoro.statemachine.pomodoro;

import eu.execom.pomodoro.statemachine.AbstractState;
import eu.execom.pomodoro.statemachine.Event;
import eu.execom.pomodoro.statemachine.StateMachine;
import eu.execom.pomodoro.timer.Timer;

public class BusyState extends AbstractState {

  private Timer busyTimer;

  public BusyState(Timer busyTimer) {
    this.busyTimer = busyTimer;
  }

  @Override
  public void onEntry() {
    super.onEntry();
    busyTimer.start();
  }

  @Override
  public void onExit() {
    super.onExit();
    busyTimer.stop();
  }

  @Override
  public void handleEvent(Event event, StateMachine stateMachine) {
    switch (event.getEventModifier()) {
    case Events.STOP:
      stateMachine.transitionTo(new InitialState());
      break;
    case Events.FINISH:
      stateMachine.transitionTo(
          new AvailableState((Timer) stateMachine.getStateResource(PomodoroStateMachine.AVAILABLE_TIMER)));
      break;
    default:
      throw new IllegalStateException(
          String.format("Cannot trigger %s from %s state", event.getEventModifier(), getClass().getName()));
    }
  }

}
