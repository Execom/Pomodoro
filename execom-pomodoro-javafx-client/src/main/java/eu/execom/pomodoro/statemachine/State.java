package eu.execom.pomodoro.statemachine;

public interface State {

  void onEntry();
  
  void execute();

  void onExit();

  void handleEvent(Event event, StateMachine stateMachine);

}
