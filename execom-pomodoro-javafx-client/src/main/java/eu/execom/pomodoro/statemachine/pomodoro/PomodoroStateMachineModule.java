package eu.execom.pomodoro.statemachine.pomodoro;

import javax.inject.Named;

import org.kordamp.jipsy.ServiceProviderFor;

import com.google.inject.AbstractModule;
import com.google.inject.Module;

import eu.execom.pomodoro.statemachine.StateMachine;

@ServiceProviderFor(Module.class)
@Named("pomodoro")
public class PomodoroStateMachineModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(StateMachine.class).to(PomodoroStateMachine.class);
  }

}
