package eu.execom.pomodoro;

import org.junit.Rule;
import org.junit.Test;

import griffon.javafx.test.GriffonTestFXRule;

public class ExecomPomodoroJavafxClientIntegrationTest {
  @Rule
  public GriffonTestFXRule testfx = new GriffonTestFXRule("mainWindow");

  @Test
  public void clickButton() {
    // // given:
    // verifyThat("#clickLabel", hasText("0"));
    //
    // // when:
    // testfx.clickOn("#clickActionTarget");
    //
    // // then:
    // verifyThat("#clickLabel", hasText("1"));
  }
}
